package com.agrognetti.garbarinoexercise;

import com.agrognetti.garbarinoexercise.contract.ProductListContract;
import com.agrognetti.garbarinoexercise.dto.ProductDTO;
import com.agrognetti.garbarinoexercise.presenter.ProductListPresenter;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;


public class ProductListPresenterTest {

    @Mock
    private ProductListContract.View view;

    @Mock
    private ProductListContract.UseCase useCase;

    @Mock
    private List<ProductDTO> productDTOS;

    private ProductListPresenter presenter;

    private ProductDTO validProduct;

    private ProductDTO invalidProduct;

    @Before
    public void setupNotesPresenter() {
        MockitoAnnotations.initMocks(this);
        CompositeDisposable compositeDisposable = new CompositeDisposable();

        validProduct = new ProductDTO();
        validProduct.setDescription("TV LG 40");
        validProduct.setListPrice(20000L);

        invalidProduct = new ProductDTO();
        invalidProduct.setDescription("");

        presenter = new ProductListPresenter(view, useCase, compositeDisposable, productDTOS);
    }

    @Test
    //Deberia devolver false si el producto es nulo, si la descripcion es nula o vacia, o si el precio de lista es igual a cero.
    public void validateProduct_InvalidProduct() {
        Assert.assertFalse(presenter.validateProduct(invalidProduct));
    }

    @Test
    //Deberia devolver true si el producto NO es nulo, si la descripcion NO es nula o vacia y si el precio de lista es mayor a cero.
    public void validateProduct_ValidProduct() {
        Assert.assertTrue(presenter.validateProduct(validProduct));
    }

    @Test
    //Chequea que el metodo addHttpToUrl efectivamente añada al string que se le provee "http" al inicio.
    public void addHttpsToUrl() {
        String exampleUrl = "//d3lfzbr90tctqz.cloudfront.net";
        String exampleUrlFormatted = presenter.addHttpToUrl(exampleUrl).substring(0, 5);
        Assert.assertEquals("http:", exampleUrlFormatted);
    }
}
