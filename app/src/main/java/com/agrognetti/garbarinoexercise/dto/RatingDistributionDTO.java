package com.agrognetti.garbarinoexercise.dto;

import com.google.gson.annotations.SerializedName;

public class RatingDistributionDTO {

    @SerializedName("rating_value")
    private long ratingValue;
    @SerializedName("count")
    private long count;


    public RatingDistributionDTO() {
    }


    public long getRatingValue() {
        return ratingValue;
    }

    public void setRatingValue(long ratingValue) {
        this.ratingValue = ratingValue;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

}
