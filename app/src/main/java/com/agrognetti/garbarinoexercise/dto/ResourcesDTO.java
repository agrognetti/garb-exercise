package com.agrognetti.garbarinoexercise.dto;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResourcesDTO {

    @SerializedName("images")
    private List<ImageDTO> images = null;
    @SerializedName("videos")
    private List<VideoDTO> videos = null;


    public ResourcesDTO() {
    }


    public List<ImageDTO> getImages() {
        return images;
    }

    public void setImages(List<ImageDTO> images) {
        this.images = images;
    }

    public List<VideoDTO> getVideos() {
        return videos;
    }

    public void setVideos(List<VideoDTO> videos) {
        this.videos = videos;
    }

}
