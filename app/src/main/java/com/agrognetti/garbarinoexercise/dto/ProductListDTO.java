package com.agrognetti.garbarinoexercise.dto;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductListDTO {

    @SerializedName("items")
    private List<ProductDTO> products = null;


    public ProductListDTO() {
    }


    public List<ProductDTO> getProducts() {
        return products;
    }

    public void setProducts(List<ProductDTO> products) {
        this.products = products;
    }

}
