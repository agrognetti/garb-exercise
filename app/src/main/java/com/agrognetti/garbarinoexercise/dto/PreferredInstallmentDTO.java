package com.agrognetti.garbarinoexercise.dto;

import com.google.gson.annotations.SerializedName;

public class PreferredInstallmentDTO {

    @SerializedName("base_price")
    private long basePrice;
    @SerializedName("installments")
    private long installments;
    @SerializedName("interest")
    private double interest;
    @SerializedName("surcharge")
    private double surcharge;
    @SerializedName("final_price")
    private double finalPrice;
    @SerializedName("installment_price")
    private double installmentPrice;
    @SerializedName("eapr")
    private double eapr;
    @SerializedName("tfc")
    private double tfc;
    @SerializedName("description")
    private String description;
    @SerializedName("gateway_installments")
    private long gatewayInstallments;
    @SerializedName("visa_financing")
    private boolean visaFinancing;
    @SerializedName("repayment")
    private long repayment;


    public PreferredInstallmentDTO() {
    }

    public long getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(long basePrice) {
        this.basePrice = basePrice;
    }

    public long getInstallments() {
        return installments;
    }

    public void setInstallments(long installments) {
        this.installments = installments;
    }

    public double getInterest() {
        return interest;
    }

    public void setInterest(double interest) {
        this.interest = interest;
    }

    public double getSurcharge() {
        return surcharge;
    }

    public void setSurcharge(double surcharge) {
        this.surcharge = surcharge;
    }

    public double getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(double finalPrice) {
        this.finalPrice = finalPrice;
    }

    public double getInstallmentPrice() {
        return installmentPrice;
    }

    public void setInstallmentPrice(double installmentPrice) {
        this.installmentPrice = installmentPrice;
    }

    public double getEapr() {
        return eapr;
    }

    public void setEapr(double eapr) {
        this.eapr = eapr;
    }

    public double getTfc() {
        return tfc;
    }

    public void setTfc(double tfc) {
        this.tfc = tfc;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getGatewayInstallments() {
        return gatewayInstallments;
    }

    public void setGatewayInstallments(long gatewayInstallments) {
        this.gatewayInstallments = gatewayInstallments;
    }

    public boolean isVisaFinancing() {
        return visaFinancing;
    }

    public void setVisaFinancing(boolean visaFinancing) {
        this.visaFinancing = visaFinancing;
    }

    public long getRepayment() {
        return repayment;
    }

    public void setRepayment(long repayment) {
        this.repayment = repayment;
    }

}
