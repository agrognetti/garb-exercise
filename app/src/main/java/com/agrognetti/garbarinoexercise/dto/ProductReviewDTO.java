package com.agrognetti.garbarinoexercise.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductReviewDTO implements Parcelable {

    public static final Creator<ProductReviewDTO> CREATOR = new Creator<ProductReviewDTO>() {
        @Override
        public ProductReviewDTO createFromParcel(Parcel in) {
            return new ProductReviewDTO(in);
        }

        @Override
        public ProductReviewDTO[] newArray(int size) {
            return new ProductReviewDTO[size];
        }
    };
    @SerializedName("id")
    private String id;
    @SerializedName("review_statistics")
    private ReviewStatisticsDTO reviewStatistics;
    @SerializedName("reviews")
    private List<ReviewDTO> reviews = null;
    @SerializedName("total_review_count")
    private long totalReviewCount;


    public ProductReviewDTO() {
    }

    protected ProductReviewDTO(Parcel in) {
        id = in.readString();
        totalReviewCount = in.readLong();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ReviewStatisticsDTO getReviewStatistics() {
        return reviewStatistics;
    }

    public void setReviewStatistics(ReviewStatisticsDTO reviewStatistics) {
        this.reviewStatistics = reviewStatistics;
    }

    public List<ReviewDTO> getReviews() {
        return reviews;
    }

    public void setReviews(List<ReviewDTO> reviews) {
        this.reviews = reviews;
    }

    public long getTotalReviewCount() {
        return totalReviewCount;
    }

    public void setTotalReviewCount(long totalReviewCount) {
        this.totalReviewCount = totalReviewCount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeLong(totalReviewCount);
    }
}
