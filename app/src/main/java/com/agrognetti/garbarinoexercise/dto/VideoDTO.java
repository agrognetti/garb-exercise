package com.agrognetti.garbarinoexercise.dto;

import com.google.gson.annotations.SerializedName;

public class VideoDTO {

    @SerializedName("url")
    private String url;
    @SerializedName("thumb_url")
    private String thumbUrl;


    public VideoDTO() {
    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }

}
