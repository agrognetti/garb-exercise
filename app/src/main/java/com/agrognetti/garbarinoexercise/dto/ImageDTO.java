package com.agrognetti.garbarinoexercise.dto;

import com.google.gson.annotations.SerializedName;

public class ImageDTO {

    @SerializedName("max_width")

    private long maxWidth;
    @SerializedName("url")

    private String url;

    /**
     * No args constructor for use in serialization
     */
    public ImageDTO() {
    }

    /**
     * @param maxWidth
     * @param url
     */
    public ImageDTO(long maxWidth, String url) {
        super();
        this.maxWidth = maxWidth;
        this.url = url;
    }

    public long getMaxWidth() {
        return maxWidth;
    }

    public void setMaxWidth(long maxWidth) {
        this.maxWidth = maxWidth;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
