package com.agrognetti.garbarinoexercise.dto;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReviewStatisticsDTO {

    @SerializedName("average_overall_rating")
    private double averageOverallRating;
    @SerializedName("rating_distribution")
    private List<RatingDistributionDTO> ratingDistribution = null;


    public ReviewStatisticsDTO() {
    }


    public double getAverageOverallRating() {
        return averageOverallRating;
    }

    public void setAverageOverallRating(double averageOverallRating) {
        this.averageOverallRating = averageOverallRating;
    }

    public List<RatingDistributionDTO> getRatingDistribution() {
        return ratingDistribution;
    }

    public void setRatingDistribution(List<RatingDistributionDTO> ratingDistribution) {
        this.ratingDistribution = ratingDistribution;
    }

}
