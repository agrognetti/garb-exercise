package com.agrognetti.garbarinoexercise.dto;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductReviewListDTO {

    @SerializedName("items")
    private List<ProductReviewDTO> productReviewList = null;

    public ProductReviewListDTO() {
    }


    public List<ProductReviewDTO> getProductReviewList() {
        return productReviewList;
    }

    public void setProductReviewList(List<ProductReviewDTO> productReviewList) {
        this.productReviewList = productReviewList;
    }

}
