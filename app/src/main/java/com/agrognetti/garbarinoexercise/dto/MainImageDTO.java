package com.agrognetti.garbarinoexercise.dto;

import com.google.gson.annotations.SerializedName;

public class MainImageDTO {

    @SerializedName("max_width")
    private long maxWidth;
    @SerializedName("url")
    private String url;


    public MainImageDTO() {
    }

    public long getMaxWidth() {
        return maxWidth;
    }

    public void setMaxWidth(long maxWidth) {
        this.maxWidth = maxWidth;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
