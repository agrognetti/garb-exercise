package com.agrognetti.garbarinoexercise.dto;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class ReviewDTO {

    @SerializedName("id")
    private String id;
    @SerializedName("usernickname")
    private String usernickname;
    @SerializedName("title")
    private String title;
    @SerializedName("review_text")
    private String reviewText;
    @SerializedName("rating")
    private long rating;
    @SerializedName("submission_time")
    private Date submissionTime;
    @SerializedName("product_id")
    private String productId;


    public ReviewDTO() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsernickname() {
        return usernickname;
    }

    public void setUsernickname(String usernickname) {
        this.usernickname = usernickname;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getReviewText() {
        return reviewText;
    }

    public void setReviewText(String reviewText) {
        this.reviewText = reviewText;
    }

    public long getRating() {
        return rating;
    }

    public void setRating(long rating) {
        this.rating = rating;
    }

    public Date getSubmissionTime() {
        return submissionTime;
    }

    public void setSubmissionTime(Date submissionTime) {
        this.submissionTime = submissionTime;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

}
