package com.agrognetti.garbarinoexercise.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductDetailDTO implements Parcelable {

    public static final Creator<ProductDetailDTO> CREATOR = new Creator<ProductDetailDTO>() {
        @Override
        public ProductDetailDTO createFromParcel(Parcel in) {
            return new ProductDetailDTO(in);
        }

        @Override
        public ProductDetailDTO[] newArray(int size) {
            return new ProductDetailDTO[size];
        }
    };
    @SerializedName("xid")
    private String xid;
    @SerializedName("description")
    private String description;
    @SerializedName("summary")
    private String summary;
    @SerializedName("brand")
    private String brand;
    @SerializedName("original_brand")
    private String originalBrand;
    @SerializedName("list_price")
    private long listPrice;
    @SerializedName("price")
    private long price;
    @SerializedName("discount")
    private long discount;
    @SerializedName("enabled_for_sale")
    private boolean enabledForSale;
    @SerializedName("preferred_installment")
    private PreferredInstallmentDTO preferredInstallment;
    @SerializedName("model")
    private String model;
    @SerializedName("category_id")
    private long categoryId;
    @SerializedName("main_image")
    private MainImageDTO mainImage;
    @SerializedName("virtual")
    private boolean virtual;
    @SerializedName("categories")
    private List<Long> categories = null;
    @SerializedName("category")
    private String category;
    @SerializedName("product_tags")
    private List<Object> productTags = null;
    @SerializedName("price_matching_discount")
    private long priceMatchingDiscount;
    @SerializedName("price_without_vat")
    private double priceWithoutVat;
    @SerializedName("vat_percentage")
    private long vatPercentage;
    @SerializedName("resources")
    private ResourcesDTO resources;


    public ProductDetailDTO() {
    }

    protected ProductDetailDTO(Parcel in) {
        xid = in.readString();
        description = in.readString();
        summary = in.readString();
        brand = in.readString();
        originalBrand = in.readString();
        listPrice = in.readLong();
        price = in.readLong();
        discount = in.readLong();
        enabledForSale = in.readByte() != 0;
        model = in.readString();
        categoryId = in.readLong();
        virtual = in.readByte() != 0;
        category = in.readString();
        priceMatchingDiscount = in.readLong();
        priceWithoutVat = in.readDouble();
        vatPercentage = in.readLong();
    }

    public String getXid() {
        return xid;
    }

    public void setXid(String xid) {
        this.xid = xid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getOriginalBrand() {
        return originalBrand;
    }

    public void setOriginalBrand(String originalBrand) {
        this.originalBrand = originalBrand;
    }

    public long getListPrice() {
        return listPrice;
    }

    public void setListPrice(long listPrice) {
        this.listPrice = listPrice;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public long getDiscount() {
        return discount;
    }

    public void setDiscount(long discount) {
        this.discount = discount;
    }

    public boolean isEnabledForSale() {
        return enabledForSale;
    }

    public void setEnabledForSale(boolean enabledForSale) {
        this.enabledForSale = enabledForSale;
    }

    public PreferredInstallmentDTO getPreferredInstallment() {
        return preferredInstallment;
    }

    public void setPreferredInstallment(PreferredInstallmentDTO preferredInstallment) {
        this.preferredInstallment = preferredInstallment;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public MainImageDTO getMainImage() {
        return mainImage;
    }

    public void setMainImage(MainImageDTO mainImage) {
        this.mainImage = mainImage;
    }

    public boolean isVirtual() {
        return virtual;
    }

    public void setVirtual(boolean virtual) {
        this.virtual = virtual;
    }

    public List<Long> getCategories() {
        return categories;
    }

    public void setCategories(List<Long> categories) {
        this.categories = categories;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<Object> getProductTags() {
        return productTags;
    }

    public void setProductTags(List<Object> productTags) {
        this.productTags = productTags;
    }

    public long getPriceMatchingDiscount() {
        return priceMatchingDiscount;
    }

    public void setPriceMatchingDiscount(long priceMatchingDiscount) {
        this.priceMatchingDiscount = priceMatchingDiscount;
    }

    public double getPriceWithoutVat() {
        return priceWithoutVat;
    }

    public void setPriceWithoutVat(double priceWithoutVat) {
        this.priceWithoutVat = priceWithoutVat;
    }

    public long getVatPercentage() {
        return vatPercentage;
    }

    public void setVatPercentage(long vatPercentage) {
        this.vatPercentage = vatPercentage;
    }

    public ResourcesDTO getResources() {
        return resources;
    }

    public void setResources(ResourcesDTO resources) {
        this.resources = resources;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(xid);
        dest.writeString(description);
        dest.writeString(summary);
        dest.writeString(brand);
        dest.writeString(originalBrand);
        dest.writeLong(listPrice);
        dest.writeLong(price);
        dest.writeLong(discount);
        dest.writeByte((byte) (enabledForSale ? 1 : 0));
        dest.writeString(model);
        dest.writeLong(categoryId);
        dest.writeByte((byte) (virtual ? 1 : 0));
        dest.writeString(category);
        dest.writeLong(priceMatchingDiscount);
        dest.writeDouble(priceWithoutVat);
        dest.writeLong(vatPercentage);
    }
}
