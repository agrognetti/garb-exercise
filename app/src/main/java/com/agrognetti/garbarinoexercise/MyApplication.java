package com.agrognetti.garbarinoexercise;

import android.app.Application;
import android.content.Context;

import com.agrognetti.garbarinoexercise.di.component.AppComponent;
import com.agrognetti.garbarinoexercise.di.component.DaggerAppComponent;

public class MyApplication extends Application {

    private AppComponent applicationComponent;

    public static MyApplication get(Context context) {
        return (MyApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        applicationComponent = DaggerAppComponent
                .builder()
                .context(getApplicationContext())
                .build();
    }


    public AppComponent getComponent() {
        return applicationComponent;
    }


}
