package com.agrognetti.garbarinoexercise.presenter;

import android.content.Context;

import com.agrognetti.garbarinoexercise.R;
import com.agrognetti.garbarinoexercise.contract.ProductDetailContract;
import com.agrognetti.garbarinoexercise.dto.ProductDetailDTO;
import com.agrognetti.garbarinoexercise.dto.ProductReviewDTO;
import com.agrognetti.garbarinoexercise.dto.ReviewDTO;
import com.agrognetti.garbarinoexercise.ui.ImageGalleryAdapter;
import com.agrognetti.garbarinoexercise.ui.UserReviewsAdapter;
import com.agrognetti.garbarinoexercise.util.StringUtil;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class ProductDetailPresenter implements ProductDetailContract.Presenter {

    private ProductDetailContract.View view;
    private ProductDetailContract.UseCase useCase;
    private CompositeDisposable compositeDisposable;

    private ProductDetailDTO productDetail;
    private ProductReviewDTO productReview;
    private List<ReviewDTO> reviews;
    private boolean imagesModeActivated = true;

    @Inject
    public ProductDetailPresenter(ProductDetailContract.View view, ProductDetailContract.UseCase useCase, CompositeDisposable compositeDisposable, List<ReviewDTO> reviews) {
        this.view = view;
        this.compositeDisposable = compositeDisposable;
        this.reviews = reviews;
        this.useCase = useCase;
    }

    @Override
    public void setProductDetail(ProductDetailDTO productDetail) {
        this.productDetail = productDetail;
    }

    @Override
    public void setProductReview(ProductReviewDTO productReview) {
        this.productReview = productReview;
    }

    @Override
    public void setReviews(List<ReviewDTO> reviews) {
        this.reviews = reviews;
    }

    @Override
    public Context getAppContext() {
        return view.getAppContext();
    }

    @Override
    public void disposeObservers() {
        compositeDisposable.dispose();
    }

    @Override
    public void retriveProductData(String productId) {
        compositeDisposable.add(useCase.getProductDetail(productId)
                .flatMap(productDetailDTO -> {
                    productDetail = productDetailDTO;
                    return useCase.getProductReviews(productId);
                })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(productReviewDTO -> {
                            productReview = productReviewDTO;
                            for (ReviewDTO review : productReview.getReviews()) {
                                if (!StringUtil.isBlank(review.getTitle(), review.getReviewText())) {
                                    reviews.add(review);
                                }
                            }
                        },
                        error -> {
                            if (isExceptionIsFromReviewsCall(error)) {
                                showProductData();
                                view.configGalleryRecyclerView();
                                view.hideProgress();
                                view.hideReviewsInfo();
                                view.showNoReviewsWarning();
                            } else {
                                handleError();
                            }
                        },
                        () -> {
                            showProductData();
                            showReviewsData();
                            view.configGalleryRecyclerView();
                            view.configReviewsRecyclerView();
                            view.hideProgress();
                        }
                ));
    }

    @Override
    public void activateImagesMode() {
        if (!imagesModeActivated) {
            if (!productDetail.getResources().getImages().isEmpty()) {
                imagesModeActivated = true;
                view.refreshGallery();
            } else {
                view.showMessage(getAppContext().getString(R.string.no_images_warning));
            }
        }
    }

    @Override
    public void activateVideosMode() {
        if (imagesModeActivated) {
            if (!productDetail.getResources().getVideos().isEmpty()) {
                imagesModeActivated = false;
                view.refreshGallery();
            } else {
                view.showMessage(getAppContext().getString(R.string.no_videos_warning));
            }
        }
    }

    @Override
    public void onBindImageRowViewAtPosition(ImageGalleryAdapter.GalleryHolderRowView holder, int position) {
        if (imagesModeActivated) {
            holder.setImage(productDetail.getResources().getImages().get(position).getUrl());
        } else {
            holder.setImage(productDetail.getResources().getVideos().get(position).getThumbUrl());
        }
    }

    @Override
    public int getImageListCount() {
        if (imagesModeActivated) {
            return productDetail.getResources().getImages().size();
        } else {
            return productDetail.getResources().getVideos().size();
        }
    }

    @Override
    public void onBindReviewRowViewAtPosition(UserReviewsAdapter.ReviewAdapterHolder holder, int position) {
        holder.setTitleReview(reviews.get(position).getTitle());
        holder.setTextReview(reviews.get(position).getReviewText());
        holder.setUserReview(reviews.get(position).getUsernickname());
        holder.setRatingReview(reviews.get(position).getRating());

        String date = StringUtil.formatDate("dd - MMMM - yyyy", reviews.get(position).getSubmissionTime()).replace("-", "de");
        holder.setDateReview(date);
    }

    @Override
    public int getReviewListCount() {
        return 3;
    }

    @Override
    public void clickedInVideo(int position) {
        if (!imagesModeActivated) {
            view.openVideo(productDetail.getResources().getVideos().get(position).getUrl());
        }
    }

    private void handleError() {
        view.onBackPressed();
        view.showMessage(getAppContext().getString(R.string.generic_error));
    }

    @Override
    public void showProductData() {
        view.setDiscount(String.valueOf(productDetail.getDiscount()));
        view.setListPrice(productDetail.getListPrice());
        view.setPrice(productDetail.getPrice());
        view.setProductDescription(productDetail.getDescription());
        view.setImagesCount(String.valueOf(productDetail.getResources().getImages().size()));
        view.setVideosCount(String.valueOf(productDetail.getResources().getVideos().size()));
    }

    @Override
    public ProductDetailDTO getProductSelected() {
        return productDetail;
    }

    @Override
    public ProductReviewDTO getProductSelectedReview() {
        return productReview;
    }

    @Override
    public void showReviewsData() {
        view.setRatingBar((float) productReview.getReviewStatistics().getAverageOverallRating());
        view.setReviewsCount(reviews.size());
    }

    private boolean isExceptionIsFromReviewsCall(Throwable error) {
        boolean result = false;
        if (error instanceof HttpException) {
            String url = ((HttpException) error).response().raw().request().url().toString();
            result = url.substring(url.length() - 7).equals("reviews");
        }
        return result;
    }
}
