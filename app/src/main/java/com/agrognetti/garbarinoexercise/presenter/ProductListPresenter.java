package com.agrognetti.garbarinoexercise.presenter;

import android.content.Context;

import com.agrognetti.garbarinoexercise.R;
import com.agrognetti.garbarinoexercise.contract.ProductListContract;
import com.agrognetti.garbarinoexercise.dto.ProductDTO;
import com.agrognetti.garbarinoexercise.ui.ProductListAdapter;
import com.agrognetti.garbarinoexercise.util.StringUtil;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class ProductListPresenter implements ProductListContract.Presenter {

    private ProductListContract.View view;
    private ProductListContract.UseCase useCase;
    private CompositeDisposable compositeDisposable;
    private List<ProductDTO> productList;


    @Inject
    public ProductListPresenter(ProductListContract.View view, ProductListContract.UseCase useCase, CompositeDisposable compositeDisposable, List<ProductDTO> productList) {
        this.view = view;
        this.useCase = useCase;
        this.compositeDisposable = compositeDisposable;
        this.productList = productList;
    }

    @Override
    public Context getAppContext() {
        return view.getAppContext();
    }

    @Override
    public void disposeObservers() {
        compositeDisposable.dispose();
    }

    @Override
    public void onBindProductRowViewAtPosition(ProductListAdapter.ProductHolderRowView holder, int position) {
        ProductDTO productDTO = productList.get(position);

        if (StringUtil.isBlank(productDTO.getDescription()) || productDTO.getListPrice() != 0) {
            holder.setDescription(productDTO.getDescription());
            try {
                holder.setImage(productDTO.getImageUrl());
            } catch (Exception e) {
                e.printStackTrace();
                view.showMessage(getAppContext().getString(R.string.image_couldnt_load));
            }

            if (productDTO.getDiscount() == 0) {
                holder.hideListPriceAndDiscount();
                holder.setPrice(productDTO.getListPrice());
            } else {
                holder.setDiscount(productDTO.getDiscount());
                holder.setPrice(productDTO.getPrice());
                holder.setListPrice(productDTO.getListPrice());
            }
        }
    }

    @Override
    public int getProductListCount() {
        return productList.size();
    }

    @Override
    public void retrieveProducts() {
        view.showProgress();
        compositeDisposable.add(useCase.getProductList()
                .filter(this::validateProduct)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(productDTO -> {
                            if (productDTO.getImageUrl() != null && !productDTO.getImageUrl().substring(0, 4).equals("http")) {
                                productDTO.setImageUrl(addHttpToUrl(productDTO.getImageUrl()));
                            }
                            productList.add(productDTO);
                        },
                        error -> handleError(),
                        () -> {
                            view.configRecyclerView();
                            view.hideProgress();
                        }));
    }

    @Override
    public void productClicked(int position) {
        view.goToProductDetail(productList.get(position).getId());
    }

    private void handleError() {
        view.hideProgress();
        view.showMessage(getAppContext().getString(R.string.generic_error));
    }

    public boolean validateProduct(ProductDTO productDTO) {
        boolean isValid = true;

        if (productDTO == null || StringUtil.isBlank(productDTO.getDescription()) || productDTO.getListPrice() <= 0) {
            isValid = false;
        }

        return isValid;
    }

    public String addHttpToUrl(String url) {
        return String.format("http:%s", url);
    }


}
