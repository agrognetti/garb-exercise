package com.agrognetti.garbarinoexercise.contract;

import android.content.Context;

import com.agrognetti.garbarinoexercise.dto.ProductDTO;
import com.agrognetti.garbarinoexercise.ui.ProductListAdapter;

import io.reactivex.Observable;

public interface ProductListContract {

    interface View {
        Context getAppContext();

        void showProgress();

        void hideProgress();

        void showMessage(String error);

        void configRecyclerView();

        void goToProductDetail(String productId);
    }

    interface Presenter {
        Context getAppContext();

        void disposeObservers();

        void onBindProductRowViewAtPosition(ProductListAdapter.ProductHolderRowView holder, int position);

        int getProductListCount();

        void retrieveProducts();

        void productClicked(int position);
    }

    interface UseCase {
        Observable<ProductDTO> getProductList();
    }
}
