package com.agrognetti.garbarinoexercise.contract;

import android.content.Context;

import com.agrognetti.garbarinoexercise.dto.ProductDetailDTO;
import com.agrognetti.garbarinoexercise.dto.ProductReviewDTO;
import com.agrognetti.garbarinoexercise.dto.ReviewDTO;
import com.agrognetti.garbarinoexercise.ui.ImageGalleryAdapter;
import com.agrognetti.garbarinoexercise.ui.UserReviewsAdapter;

import java.util.List;

import io.reactivex.Observable;

public interface ProductDetailContract {

    interface View {
        Context getAppContext();

        void showProgress();

        void hideProgress();

        void showMessage(String error);

        void setRatingBar(float rating);

        void setProductDescription(String productDescription);

        void setReviewsCount(int reviewsCount);

        void setPrice(long price);

        void setListPrice(long listPrice);

        void setDiscount(String discount);

        void onBackPressed();

        void configGalleryRecyclerView();

        void configReviewsRecyclerView();

        void hideReviewsInfo();

        void showNoReviewsWarning();

        void setImagesCount(String imagesCount);

        void setVideosCount(String videosCount);

        void refreshGallery();

        void openVideo(String url);
    }

    interface Presenter {
        void setProductDetail(ProductDetailDTO productDetail);

        void setProductReview(ProductReviewDTO productReview);

        void setReviews(List<ReviewDTO> reviews);

        Context getAppContext();

        void disposeObservers();

        void retriveProductData(String productId);

        void activateImagesMode();

        void activateVideosMode();

        void onBindImageRowViewAtPosition(ImageGalleryAdapter.GalleryHolderRowView holder, int position);

        int getImageListCount();

        void onBindReviewRowViewAtPosition(UserReviewsAdapter.ReviewAdapterHolder holder, int position);

        int getReviewListCount();

        void clickedInVideo(int position);

        void showProductData();

        ProductDetailDTO getProductSelected();

        ProductReviewDTO getProductSelectedReview();

        void showReviewsData();
    }

    interface UseCase {
        Observable<ProductDetailDTO> getProductDetail(String id);

        Observable<ProductReviewDTO> getProductReviews(String id);
    }
}
