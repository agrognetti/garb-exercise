package com.agrognetti.garbarinoexercise.di.component;

import android.content.Context;

import com.agrognetti.garbarinoexercise.api.RetrofitService;
import com.agrognetti.garbarinoexercise.di.modules.RetrofitModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

@Singleton
@Component(modules = RetrofitModule.class)
public interface AppComponent {

    Context getContext();

    RetrofitService getRetrofitService();

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder context(Context applicationContext);

        AppComponent build();
    }
}
