package com.agrognetti.garbarinoexercise.di.component;

import com.agrognetti.garbarinoexercise.contract.ProductDetailContract;
import com.agrognetti.garbarinoexercise.di.annotations.PerActivity;
import com.agrognetti.garbarinoexercise.di.modules.ProductDetailModule;
import com.agrognetti.garbarinoexercise.ui.ProductDetailActivity;

import dagger.BindsInstance;
import dagger.Component;

@PerActivity
@Component(dependencies = AppComponent.class, modules = {ProductDetailModule.class})
public interface ProductDetailComponent {

    void inject(ProductDetailActivity productDetailActivity);

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder view(ProductDetailContract.View view);

        Builder appComponent(AppComponent appComponent);

        ProductDetailComponent build();
    }
}
