package com.agrognetti.garbarinoexercise.di.modules;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.StaggeredGridLayoutManager;

import com.agrognetti.garbarinoexercise.api.RetrofitService;
import com.agrognetti.garbarinoexercise.contract.ProductListContract;
import com.agrognetti.garbarinoexercise.di.annotations.PerActivity;
import com.agrognetti.garbarinoexercise.dto.ProductDTO;
import com.agrognetti.garbarinoexercise.presenter.ProductListPresenter;
import com.agrognetti.garbarinoexercise.ui.ProductDetailActivity;
import com.agrognetti.garbarinoexercise.ui.ProductListAdapter;
import com.agrognetti.garbarinoexercise.useCase.ProductListUseCase;

import java.util.ArrayList;
import java.util.List;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Module
public class ProductListModule {

    @Provides
    List<ProductDTO> providesProductDTOList() {
        return new ArrayList<>();
    }

    @Provides
    CompositeDisposable providesCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    @PerActivity
    ProductListContract.UseCase providesProductListUseCase(RetrofitService retrofitService) {
        return new ProductListUseCase(retrofitService);
    }

    @Provides
    @PerActivity
    ProductListContract.Presenter providesProductListPresenter(ProductListContract.View view, ProductListContract.UseCase useCase,
                                                               CompositeDisposable compositeDisposable, List<ProductDTO> productDTOList) {
        return new ProductListPresenter(view, useCase, compositeDisposable, productDTOList);
    }

    @Provides
    StaggeredGridLayoutManager provideStaggeredGridLayoutManager() {
        return new StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL);
    }

    @Provides
    ProductListAdapter provideProductListRecyclerAdapter(ProductListContract.Presenter presenter) {
        return new ProductListAdapter(presenter, (v, position) -> presenter.productClicked(position));
    }

    @Provides
    Intent provideIntentToProductDetail(Activity activity) {
        return new Intent(activity, ProductDetailActivity.class);
    }
}
