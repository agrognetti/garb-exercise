package com.agrognetti.garbarinoexercise.di.modules;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.SnapHelper;

import com.agrognetti.garbarinoexercise.api.RetrofitService;
import com.agrognetti.garbarinoexercise.contract.ProductDetailContract;
import com.agrognetti.garbarinoexercise.di.annotations.PerActivity;
import com.agrognetti.garbarinoexercise.dto.ReviewDTO;
import com.agrognetti.garbarinoexercise.presenter.ProductDetailPresenter;
import com.agrognetti.garbarinoexercise.ui.ImageGalleryAdapter;
import com.agrognetti.garbarinoexercise.ui.UserReviewsAdapter;
import com.agrognetti.garbarinoexercise.useCase.ProductDetailUseCase;

import java.util.ArrayList;
import java.util.List;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Module
public class ProductDetailModule {

    @Provides
    List<ReviewDTO> providesReviewDTOList() {
        return new ArrayList<>();
    }

    @Provides
    CompositeDisposable providesCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    @PerActivity
    ProductDetailContract.UseCase providesProductListUseCase(RetrofitService retrofitService) {
        return new ProductDetailUseCase(retrofitService);
    }

    @Provides
    @PerActivity
    ProductDetailContract.Presenter providesProductDetailPresenter(ProductDetailContract.View view, ProductDetailContract.UseCase useCase,
                                                                   CompositeDisposable compositeDisposable, List<ReviewDTO> reviews) {
        return new ProductDetailPresenter(view, useCase, compositeDisposable, reviews);
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(Context appContext) {
        return new LinearLayoutManager(appContext);
    }

    @Provides
    SnapHelper provideSnapHelper() {
        return new PagerSnapHelper();
    }

    @Provides
    UserReviewsAdapter providesUserReviewsAdapter(ProductDetailContract.Presenter presenter) {
        return new UserReviewsAdapter(presenter);
    }

    @Provides
    ImageGalleryAdapter providesImageGalleryAdapter(ProductDetailContract.Presenter presenter) {
        return new ImageGalleryAdapter(presenter, (v, position) -> presenter.clickedInVideo(position));
    }

    @Provides
    Intent providesIntent() {
        return new Intent();
    }
}
