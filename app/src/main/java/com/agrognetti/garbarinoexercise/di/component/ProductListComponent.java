package com.agrognetti.garbarinoexercise.di.component;

import android.app.Activity;

import com.agrognetti.garbarinoexercise.contract.ProductListContract;
import com.agrognetti.garbarinoexercise.di.annotations.PerActivity;
import com.agrognetti.garbarinoexercise.di.modules.ProductListModule;
import com.agrognetti.garbarinoexercise.ui.ProductListActivity;

import dagger.BindsInstance;
import dagger.Component;

@PerActivity
@Component(dependencies = AppComponent.class, modules = {ProductListModule.class})
public interface ProductListComponent {

    void inject(ProductListActivity productListActivity);

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder view(ProductListContract.View view);

        @BindsInstance
        Builder activity(Activity activity);

        Builder appComponent(AppComponent appComponent);

        ProductListComponent build();
    }
}
