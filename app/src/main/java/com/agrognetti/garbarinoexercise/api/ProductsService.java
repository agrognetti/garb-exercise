package com.agrognetti.garbarinoexercise.api;

import com.agrognetti.garbarinoexercise.dto.ProductDetailDTO;
import com.agrognetti.garbarinoexercise.dto.ProductListDTO;
import com.agrognetti.garbarinoexercise.dto.ProductReviewListDTO;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ProductsService {

    @GET("products")
    Observable<ProductListDTO> fetchAllProducts();

    @GET("products/{id}")
    Observable<ProductDetailDTO> fetchProductDetail(
            @Path("id") String id
    );

    @GET("products/{id}/reviews")
    Observable<ProductReviewListDTO> fetchProductReview(
            @Path("id") String id
    );
}
