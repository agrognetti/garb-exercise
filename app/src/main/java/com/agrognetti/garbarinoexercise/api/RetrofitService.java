package com.agrognetti.garbarinoexercise.api;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Retrofit;

@Singleton
public class RetrofitService {

    private Retrofit retrofit;
    private ProductsService productsService;

    @Inject
    public RetrofitService(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    @Inject
    void createApis() {
        productsService = retrofit.create(ProductsService.class);
    }

    public ProductsService getProductsService() {
        return productsService;
    }
}
