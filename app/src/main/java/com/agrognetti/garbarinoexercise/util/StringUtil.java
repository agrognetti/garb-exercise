package com.agrognetti.garbarinoexercise.util;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Agustin Grognetti on 14/11/2017.
 */

public abstract class StringUtil {

    public static Boolean isBlank(String... strings) {
        Boolean result = false;

        for (String value : strings) {
            if (value == null || value.length() <= 0) {
                result = true;
                break;
            }
        }

        return result;
    }

    public static String formatToMoney(long amount) {
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance();
        numberFormat.setMaximumFractionDigits(0);
        return numberFormat.format(amount);
    }

    public static String formatDate(String pattern, Date date) {
        DateFormat dateFormat = new SimpleDateFormat(pattern, Locale.getDefault());
        return dateFormat.format(date);
    }
}
