package com.agrognetti.garbarinoexercise.ui;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.agrognetti.garbarinoexercise.R;
import com.agrognetti.garbarinoexercise.contract.ProductDetailContract;

public class UserReviewsAdapter extends RecyclerView.Adapter<UserReviewsAdapter.ReviewAdapterHolder> {

    private ProductDetailContract.Presenter presenter;

    public UserReviewsAdapter(ProductDetailContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public ReviewAdapterHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.holder_review_recycler, viewGroup, false);

        return new ReviewAdapterHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewAdapterHolder reviewAdapterHolder, int i) {
        presenter.onBindReviewRowViewAtPosition(reviewAdapterHolder, i);
    }

    @Override
    public int getItemCount() {
        return presenter.getReviewListCount();
    }

    public interface ReviewHolderRowView {

        void setDateReview(String date);

        void setTitleReview(String titleReview);

        void setTextReview(String textReview);

        void setUserReview(String userReview);

        void setRatingReview(float ratingReview);
    }

    public class ReviewAdapterHolder extends RecyclerView.ViewHolder implements ReviewHolderRowView {

        private TextView dateReview, titleReview, textReview, userReview;
        private RatingBar ratingBar;

        public ReviewAdapterHolder(@NonNull View itemView) {
            super(itemView);
            findViews(itemView);
        }

        private void findViews(View itemView) {
            dateReview = itemView.findViewById(R.id.date_review);
            titleReview = itemView.findViewById(R.id.title_review);
            textReview = itemView.findViewById(R.id.text_review);
            userReview = itemView.findViewById(R.id.user_nick_name_review);
            ratingBar = itemView.findViewById(R.id.ratingBar1);
        }

        @Override
        public void setDateReview(String date) {
            dateReview.setText(date);
        }

        @Override
        public void setTitleReview(String titleReview) {
            this.titleReview.setText(titleReview);
        }

        @Override
        public void setTextReview(String textReview) {
            this.textReview.setText(textReview);
        }

        @Override
        public void setUserReview(String userReview) {
            userReview = String.format("Por %s", userReview);
            SpannableString ss = new SpannableString(userReview);
            ForegroundColorSpan fcsBlack = new ForegroundColorSpan(Color.BLACK);
            ss.setSpan(fcsBlack, 3, userReview.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            this.userReview.setText(ss);
        }

        @Override
        public void setRatingReview(float ratingReview) {
            ratingBar.setRating(ratingReview);
        }
    }
}
