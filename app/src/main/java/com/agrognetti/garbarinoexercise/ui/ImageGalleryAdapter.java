package com.agrognetti.garbarinoexercise.ui;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.agrognetti.garbarinoexercise.R;
import com.agrognetti.garbarinoexercise.contract.ProductDetailContract;
import com.bumptech.glide.Glide;

public class ImageGalleryAdapter extends RecyclerView.Adapter<ImageGalleryAdapter.ImageGalleryViewHolder> {

    private ProductDetailContract.Presenter presenter;
    private ProductListAdapter.ItemClickListener itemClickListener;

    public ImageGalleryAdapter(ProductDetailContract.Presenter presenter, ProductListAdapter.ItemClickListener itemClickListener) {
        this.presenter = presenter;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public ImageGalleryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.holder_product_gallery_recycler, viewGroup, false);

        ImageGalleryViewHolder holder = new ImageGalleryViewHolder(view);
        view.setOnClickListener(v1 -> itemClickListener.onItemClick(v1, holder.getAdapterPosition()));


        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ImageGalleryViewHolder imageGalleryViewHolder, int i) {
        presenter.onBindImageRowViewAtPosition(imageGalleryViewHolder, i);
    }

    @Override
    public int getItemCount() {
        return presenter.getImageListCount();
    }

    public interface GalleryHolderRowView {
        void setImage(String imageUrl);
    }

    public class ImageGalleryViewHolder extends RecyclerView.ViewHolder implements GalleryHolderRowView {

        private ImageView imageGallery;

        public ImageGalleryViewHolder(@NonNull View itemView) {
            super(itemView);
            findViews(itemView);
        }

        private void findViews(View itemView) {
            imageGallery = itemView.findViewById(R.id.gallery_image);
        }

        @Override
        public void setImage(String imageUrl) {
            if (!imageUrl.substring(0, 4).equals("http")) {
                imageUrl = String.format("http:%s", imageUrl);
            }

            Glide.with(presenter.getAppContext())
                    .load(imageUrl)
                    .into(imageGallery);
        }
    }
}
