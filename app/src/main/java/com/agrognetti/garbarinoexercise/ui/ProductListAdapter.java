package com.agrognetti.garbarinoexercise.ui;

import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.agrognetti.garbarinoexercise.R;
import com.agrognetti.garbarinoexercise.contract.ProductListContract;
import com.agrognetti.garbarinoexercise.util.StringUtil;
import com.bumptech.glide.Glide;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ProductViewHolder> {

    private ProductListContract.Presenter presenter;
    private ItemClickListener productClickListener;


    public ProductListAdapter(ProductListContract.Presenter presenter, ItemClickListener productClickListener) {
        this.presenter = presenter;
        this.productClickListener = productClickListener;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.holder_product_list_recycler, viewGroup, false);

        ProductViewHolder holder = new ProductViewHolder(view);
        view.setOnClickListener(v1 -> productClickListener.onItemClick(v1, holder.getAdapterPosition()));

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder productViewHolder, int i) {
        presenter.onBindProductRowViewAtPosition(productViewHolder, i);
    }

    @Override
    public int getItemCount() {
        return presenter.getProductListCount();
    }

    public interface ProductHolderRowView {

        void setImage(String imageUrl) throws Exception;

        void setDescription(String description);

        void setPrice(long price);

        void setListPrice(long listPrice);

        void setDiscount(long discount);

        void hideListPriceAndDiscount();
    }

    public interface ItemClickListener {
        void onItemClick(View v, int position);
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder implements ProductHolderRowView {

        private TextView description, price, listPrice, discount;
        private ImageView imageProduct;

        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);
            findViews(itemView);
            styleViews();
        }

        private void findViews(View itemView) {
            description = itemView.findViewById(R.id.description_product);
            price = itemView.findViewById(R.id.price);
            listPrice = itemView.findViewById(R.id.list_price);
            discount = itemView.findViewById(R.id.discount);
            imageProduct = itemView.findViewById(R.id.image_product);
        }

        private void styleViews() {
            listPrice.setPaintFlags(listPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }

        @Override
        public void setImage(String imageUrl) {
            Glide.with(presenter.getAppContext())
                    .load(imageUrl)
                    .fitCenter()
                    .override(250, 250)
                    .into(imageProduct);
        }

        @Override
        public void setDescription(String description) {
            this.description.setText(description);
        }

        @Override
        public void setPrice(long price) {
            this.price.setText(StringUtil.formatToMoney(price));
        }

        @Override
        public void setListPrice(long listPrice) {
            this.listPrice.setText(StringUtil.formatToMoney(listPrice));
        }

        @Override
        public void setDiscount(long discount) {
            this.discount.setText(String.format("%s%% OFF", String.valueOf(discount)));
        }

        @Override
        public void hideListPriceAndDiscount() {
            listPrice.setVisibility(View.GONE);
            discount.setVisibility(View.GONE);
        }
    }
}
