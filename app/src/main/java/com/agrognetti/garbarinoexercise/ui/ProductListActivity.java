package com.agrognetti.garbarinoexercise.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.agrognetti.garbarinoexercise.MyApplication;
import com.agrognetti.garbarinoexercise.R;
import com.agrognetti.garbarinoexercise.contract.ProductListContract;
import com.agrognetti.garbarinoexercise.di.component.DaggerProductListComponent;
import com.agrognetti.garbarinoexercise.di.component.ProductListComponent;

import javax.inject.Inject;

import static com.agrognetti.garbarinoexercise.ui.ProductDetailActivity.PRODUCT_SELECTED_ID_KEY;

public class ProductListActivity extends AppCompatActivity implements ProductListContract.View {

    @Inject
    ProductListContract.Presenter presenter;
    @Inject
    StaggeredGridLayoutManager staggeredGridLayoutManager;
    @Inject
    ProductListAdapter adapter;
    @Inject
    Intent intentToProductDetail;

    private RelativeLayout progressBar;
    private RecyclerView productListRecyclerView;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        findViews();
        styleViews();
        configDagger();
        presenter.retrieveProducts();
    }

    private void configDagger() {
        ProductListComponent component = DaggerProductListComponent
                .builder()
                .view(this)
                .activity(this)
                .appComponent(((MyApplication) getApplication()).getComponent())
                .build();

        component.inject(this);
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    private void findViews() {
        progressBar = findViewById(R.id.layout_progress_bar);
        productListRecyclerView = findViewById(R.id.product_list_recycler_view);
        toolbar = findViewById(R.id.toolbar);
    }

    private void styleViews() {
        toolbar.setTitle(getString(R.string.product_list_title));
        setSupportActionBar(toolbar);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showMessage(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void configRecyclerView() {
        productListRecyclerView.setLayoutManager(staggeredGridLayoutManager);
        productListRecyclerView.setAdapter(adapter);
    }

    @Override
    public void goToProductDetail(String productId) {
        intentToProductDetail.putExtra(PRODUCT_SELECTED_ID_KEY, productId);
        startActivity(intentToProductDetail);
    }

    @Override
    protected void onDestroy() {
        presenter.disposeObservers();
        super.onDestroy();
    }
}
