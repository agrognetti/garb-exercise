package com.agrognetti.garbarinoexercise.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.agrognetti.garbarinoexercise.MyApplication;
import com.agrognetti.garbarinoexercise.R;
import com.agrognetti.garbarinoexercise.contract.ProductDetailContract;
import com.agrognetti.garbarinoexercise.di.component.DaggerProductDetailComponent;
import com.agrognetti.garbarinoexercise.di.component.ProductDetailComponent;
import com.agrognetti.garbarinoexercise.dto.ProductDetailDTO;
import com.agrognetti.garbarinoexercise.dto.ProductReviewDTO;
import com.agrognetti.garbarinoexercise.util.StringUtil;

import javax.inject.Inject;

public class ProductDetailActivity extends AppCompatActivity implements ProductDetailContract.View {

    public static final String PRODUCT_SELECTED_ID_KEY = "product_selected_id";
    public static final String PRODUCT_SELECTED_KEY = "product_selected";
    private static final String PRODUCT_SELECTED_REVIEW_KEY = "product_selected_review";

    @Inject
    LinearLayoutManager layoutManagerImageGallery, layoutManagerReviews;
    @Inject
    SnapHelper snapHelper;
    @Inject
    ImageGalleryAdapter imageGalleryAdapter;
    @Inject
    ProductDetailContract.Presenter presenter;
    @Inject
    UserReviewsAdapter userReviewsAdapter;
    @Inject
    Intent openVideoIntent;

    private RelativeLayout progressBar, reviewsInfoContainer;
    private RecyclerView imageGalleryRecycler, reviewsRecycler;
    private TextView description, price, listPrice, discount, rating, averageRatingTxt, noReviewsWarning, imagesCount, videosCount;
    private ImageView imagesIcon, videosIcon;
    private RatingBar ratingBar, ratingBar2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        ProductDetailDTO productSelected = (savedInstanceState != null) ? savedInstanceState.getParcelable(PRODUCT_SELECTED_KEY) : null;
        ProductReviewDTO productSelectedReview = (savedInstanceState != null) ? savedInstanceState.getParcelable(PRODUCT_SELECTED_REVIEW_KEY) : null;
        String productId = (getIntent().getExtras() != null) ? getIntent().getExtras().getString(PRODUCT_SELECTED_ID_KEY) : null;

        configDagger();
        findViews();
        showProgress();
        styleViews();
        setListeners();

        if (productSelected != null && productSelectedReview != null) {
            presenter.setProductDetail(productSelected);
            presenter.setProductReview(productSelectedReview);
            presenter.setReviews(productSelectedReview.getReviews());
            presenter.showProductData();
            presenter.showReviewsData();
            configGalleryRecyclerView();
            configReviewsRecyclerView();
            hideProgress();
        } else if (productId != null) {
            presenter.retriveProductData(productId);
        }
    }

    private void configDagger() {
        ProductDetailComponent component = DaggerProductDetailComponent
                .builder()
                .view(this)
                .appComponent(((MyApplication) getApplication()).getComponent())
                .build();

        component.inject(this);
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    private void findViews() {
        progressBar = findViewById(R.id.layout_progress_bar);
        reviewsInfoContainer = findViewById(R.id.reviews_info_container);
        imageGalleryRecycler = findViewById(R.id.image_gallery_recycler_view);
        reviewsRecycler = findViewById(R.id.reviews_recycler_view);

        description = findViewById(R.id.description_product);
        price = findViewById(R.id.price);
        listPrice = findViewById(R.id.list_price);
        discount = findViewById(R.id.discount);
        rating = findViewById(R.id.rating);
        averageRatingTxt = findViewById(R.id.average_rating_text);
        noReviewsWarning = findViewById(R.id.no_reviews_warning);
        imagesCount = findViewById(R.id.images_count);
        videosCount = findViewById(R.id.videos_count);

        ratingBar = findViewById(R.id.ratingBar1);
        ratingBar2 = findViewById(R.id.ratingBar2);

        imagesIcon = findViewById(R.id.images_icon);
        videosIcon = findViewById(R.id.videos_icon);
    }

    private void styleViews() {
        listPrice.setPaintFlags(listPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        configRatingBar(ratingBar);
        configRatingBar(ratingBar2);

        Drawable ratingIcon = getDrawable(R.drawable.custom_rate_image);
        if (ratingIcon != null)
            ratingIcon.setColorFilter(ContextCompat.getColor(getAppContext(), R.color.gold), PorterDuff.Mode.SRC_IN);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.product_detail_title));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void setListeners() {
        imagesIcon.setOnClickListener(v -> presenter.activateImagesMode());
        imagesCount.setOnClickListener(v -> presenter.activateImagesMode());
        videosIcon.setOnClickListener(v -> presenter.activateVideosMode());
        videosCount.setOnClickListener(v -> presenter.activateVideosMode());
    }

    private void configRatingBar(RatingBar ratingBar) {
        ratingBar.setIsIndicator(true);
        ratingBar.setNumStars(5);
        ratingBar.setMax(5);
        ratingBar.setStepSize(0.5f);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showMessage(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setRatingBar(float rating) {
        ratingBar.setRating(rating);
        ratingBar2.setRating(rating);
        this.rating.setText(String.valueOf(rating));
    }

    @Override
    public void setProductDescription(String productDescription) {
        description.setText(productDescription);
    }

    @Override
    public void setReviewsCount(int reviewsCount) {
        averageRatingTxt.setText(getString(R.string.average_rating_text, reviewsCount));
    }

    @Override
    public void setPrice(long price) {
        this.price.setText(StringUtil.formatToMoney(price));
    }

    @Override
    public void setListPrice(long listPrice) {
        this.listPrice.setText(StringUtil.formatToMoney(listPrice));
    }

    @Override
    public void setDiscount(String discount) {
        this.discount.setText(String.format("%s%% OFF", discount));
    }

    @Override
    public void configGalleryRecyclerView() {
        layoutManagerImageGallery.setOrientation(LinearLayoutManager.HORIZONTAL);
        imageGalleryRecycler.setLayoutManager(layoutManagerImageGallery);
        snapHelper.attachToRecyclerView(imageGalleryRecycler);
        imageGalleryRecycler.setAdapter(imageGalleryAdapter);
    }

    @Override
    public void configReviewsRecyclerView() {
        layoutManagerReviews.setOrientation(LinearLayoutManager.VERTICAL);
        reviewsRecycler.setLayoutManager(layoutManagerReviews);
        reviewsRecycler.setAdapter(userReviewsAdapter);
    }

    @Override
    public void hideReviewsInfo() {
        reviewsInfoContainer.setVisibility(View.GONE);
    }

    @Override
    public void showNoReviewsWarning() {
        noReviewsWarning.setVisibility(View.VISIBLE);
    }

    @Override
    public void setImagesCount(String imagesCount) {
        this.imagesCount.setText(imagesCount);
    }

    @Override
    public void setVideosCount(String videosCount) {
        this.videosCount.setText(videosCount);
    }

    @Override
    public void refreshGallery() {
        imageGalleryAdapter.notifyDataSetChanged();
    }

    @Override
    public void openVideo(String url) {
        openVideoIntent.setAction(Intent.ACTION_VIEW);
        openVideoIntent.setData(Uri.parse(url));
        startActivity(openVideoIntent);
    }

    @Override
    protected void onDestroy() {
        presenter.disposeObservers();
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(PRODUCT_SELECTED_KEY, presenter.getProductSelected());
        outState.putParcelable(PRODUCT_SELECTED_REVIEW_KEY, presenter.getProductSelectedReview());
    }
}
