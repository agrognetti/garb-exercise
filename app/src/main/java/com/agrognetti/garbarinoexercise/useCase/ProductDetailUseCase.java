package com.agrognetti.garbarinoexercise.useCase;

import com.agrognetti.garbarinoexercise.api.RetrofitService;
import com.agrognetti.garbarinoexercise.contract.ProductDetailContract;
import com.agrognetti.garbarinoexercise.dto.ProductDetailDTO;
import com.agrognetti.garbarinoexercise.dto.ProductReviewDTO;

import javax.inject.Inject;

import io.reactivex.Observable;

public class ProductDetailUseCase implements ProductDetailContract.UseCase {

    private RetrofitService retrofitService;

    @Inject
    public ProductDetailUseCase(RetrofitService retrofitService) {
        this.retrofitService = retrofitService;
    }

    @Override
    public Observable<ProductDetailDTO> getProductDetail(String id) {
        return retrofitService.getProductsService().fetchProductDetail(id);
    }

    @Override
    public Observable<ProductReviewDTO> getProductReviews(String id) {
        return retrofitService.getProductsService().fetchProductReview(id).flatMap(productReviewListDTO ->
                Observable.fromIterable(productReviewListDTO.getProductReviewList()));
    }

}
