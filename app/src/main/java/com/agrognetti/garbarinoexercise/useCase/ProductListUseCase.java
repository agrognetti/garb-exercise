package com.agrognetti.garbarinoexercise.useCase;

import com.agrognetti.garbarinoexercise.api.RetrofitService;
import com.agrognetti.garbarinoexercise.contract.ProductListContract;
import com.agrognetti.garbarinoexercise.dto.ProductDTO;

import javax.inject.Inject;

import io.reactivex.Observable;

public class ProductListUseCase implements ProductListContract.UseCase {

    private RetrofitService retrofitService;

    @Inject
    public ProductListUseCase(RetrofitService retrofitService) {
        this.retrofitService = retrofitService;
    }

    @Override
    public Observable<ProductDTO> getProductList() {
        return retrofitService.getProductsService().fetchAllProducts().flatMap(productListDTO ->
                Observable.fromIterable(productListDTO.getProducts()));
    }

}
